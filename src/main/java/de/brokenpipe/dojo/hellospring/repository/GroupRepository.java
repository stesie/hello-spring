package de.brokenpipe.dojo.hellospring.repository;

import de.brokenpipe.dojo.hellospring.model.Contact;
import de.brokenpipe.dojo.hellospring.model.Group;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface GroupRepository extends CrudRepository<Group, Integer> {
    Set<Group> findByContactsContains(Contact contact);
}
