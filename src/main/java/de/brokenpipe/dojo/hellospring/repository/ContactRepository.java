package de.brokenpipe.dojo.hellospring.repository;

import de.brokenpipe.dojo.hellospring.model.Contact;
import de.brokenpipe.dojo.hellospring.model.Group;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface ContactRepository extends CrudRepository<Contact, Integer> {
    Set<Contact> findByGroupsContains(Group group);
}
