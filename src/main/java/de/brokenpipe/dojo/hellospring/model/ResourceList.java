package de.brokenpipe.dojo.hellospring.model;

public class ResourceList<T> {
    private Iterable<T> resources;

    public ResourceList(Iterable<T> resources) {
        this.resources = resources;
    }

    public Iterable<T> getResources() {
        return resources;
    }
}
