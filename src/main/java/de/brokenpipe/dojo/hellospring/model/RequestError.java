package de.brokenpipe.dojo.hellospring.model;

public class RequestError {
    private String message;

    private RequestError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    static public RequestError nameNotUnique() {
        return new RequestError("Name is not unique");
    }
}
