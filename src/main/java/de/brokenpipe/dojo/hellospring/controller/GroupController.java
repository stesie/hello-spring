package de.brokenpipe.dojo.hellospring.controller;

import de.brokenpipe.dojo.hellospring.model.Contact;
import de.brokenpipe.dojo.hellospring.model.Group;
import de.brokenpipe.dojo.hellospring.model.RequestError;
import de.brokenpipe.dojo.hellospring.model.ResourceList;
import de.brokenpipe.dojo.hellospring.repository.ContactRepository;
import de.brokenpipe.dojo.hellospring.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;

@Controller
@RequestMapping(path = "/groups")
public class GroupController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ContactRepository contactRepository;

    @GetMapping(path = "")
    public @ResponseBody
    ResourceList<Group> listGroups() {
        return new ResourceList<>(groupRepository.findAll());
    }

    @GetMapping(path = "/{group}")
    public @ResponseBody
    ResponseEntity<Group> getGroup(@PathVariable Group group) {
        if (group == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(group);
    }

    @DeleteMapping(path = "/{group}")
    public @ResponseBody
    ResponseEntity<Void> deleteGroup(@PathVariable Group group) {
        if (group == null) {
            return ResponseEntity.notFound().build();
        }

        groupRepository.delete(group);
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "")
    public @ResponseBody
    ResponseEntity<?> createGroup(@RequestBody Group group) {
        group.setId(null);

        try {
            groupRepository.save(group);
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.badRequest().body(RequestError.nameNotUnique());
        }

        URI uri = MvcUriComponentsBuilder.fromMethodCall(
                MvcUriComponentsBuilder.on(GroupController.class).getGroup(group)
        ).build().toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping(path = "/{group}/contacts/")
    public @ResponseBody
    ResponseEntity<ResourceList<Contact>> getContactsByGroup(@PathVariable Group group) {
        if (group == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new ResourceList<>(contactRepository.findByGroupsContains(group)));
    }

    @PostMapping(path = "/{group}/contacts/{contact}")
    public @ResponseBody
    ResponseEntity<Void> addContactToGroup(@PathVariable Group group, @PathVariable Contact contact) {
        if (group == null || contact == null) {
            return ResponseEntity.notFound().build();
        }

        group.addContact(contact);
        groupRepository.save(group);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{group}/contacts/{contact}")
    public @ResponseBody
    ResponseEntity<Void> removeContactFromGroup(@PathVariable Group group, @PathVariable Contact contact) {
        if (group == null || contact == null) {
            return ResponseEntity.notFound().build();
        }

        if (!group.getContacts().contains(contact)) {
            return ResponseEntity.badRequest().build();
        }

        group.removeContact(contact);
        groupRepository.save(group);

        return ResponseEntity.ok().build();
    }
}
