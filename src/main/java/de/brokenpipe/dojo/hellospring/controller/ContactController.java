package de.brokenpipe.dojo.hellospring.controller;

import de.brokenpipe.dojo.hellospring.model.Contact;
import de.brokenpipe.dojo.hellospring.model.Group;
import de.brokenpipe.dojo.hellospring.model.ResourceList;
import de.brokenpipe.dojo.hellospring.repository.ContactRepository;
import de.brokenpipe.dojo.hellospring.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;

@Controller
@RequestMapping(path = "/contacts")
public class ContactController {
    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private GroupRepository groupRepository;

    @GetMapping(path = "/")
    public @ResponseBody
    ResourceList<Contact> listContacts() {
        return new ResourceList<>(contactRepository.findAll());
    }

    @GetMapping(path = "/{contact}")
    public @ResponseBody
    ResponseEntity<Contact> getContact(@PathVariable(name = "contact") Contact contact) {
        if (contact == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(contact);
    }

    @PostMapping(path = "/")
    public @ResponseBody
    ResponseEntity<Void> addContact(@RequestBody Contact contact) {
        // don't allow remote to set our database ID
        contact.setId(null);

        try {
            contactRepository.save(contact);
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.badRequest().build();
        }

        URI uri = MvcUriComponentsBuilder.fromMethodCall(
                MvcUriComponentsBuilder.on(ContactController.class).getContact(contact)
        ).build().toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping(path = "/{contact}")
    public @ResponseBody
    ResponseEntity<Void> deleteContact(@PathVariable("contact") Contact contact) {
        if (contact == null) {
            return ResponseEntity.notFound().build();
        }

        contactRepository.delete(contact);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{contact}/groups/")
    public @ResponseBody
    ResponseEntity<ResourceList<Group>> getGroupsOfContact(@PathVariable Contact contact) {
        if (contact == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new ResourceList<>(groupRepository.findByContactsContains(contact)));
    }
}
